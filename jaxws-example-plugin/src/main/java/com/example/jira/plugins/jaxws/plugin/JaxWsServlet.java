package com.example.jira.plugins.jaxws.plugin;

import com.example.jira.plugins.jaxws.server.HelloWorldService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

/**
 * A simple servlet that servers as a proof-of-concept for embedding JAX-WS in JIRA. Hitting this servlet will cause the
 * plugin to make a client call to a JAX-WS endpoint (see the jaxws-example-server module).
 */
public class JaxWsServlet extends HttpServlet
{
    private static final String URL_STRING = "http://localhost:9999/hello?wsdl";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        String name = req.getParameter("name");
        if (name == null)
            name = "";

        URL url = new URL(URL_STRING);
        QName qname = new QName("http://server.jaxws.plugins.jira.example.com/", "HelloWorldServiceImplService");
        Service service = Service.create(url, qname);

        HelloWorldService hello = service.getPort(HelloWorldService.class);

        String result = "";
        ClassLoader original = Thread.currentThread().getContextClassLoader();
        try
        {
            Thread.currentThread().setContextClassLoader(new OverridingClassLoader(original));
            result = hello.sayHello(name);
        }
        finally
        {
            Thread.currentThread().setContextClassLoader(original);
        }

        resp.setContentType("text/html");
        PrintWriter writer = resp.getWriter();

        writer.write("<html><head><title>Hello World, JAX-WS Style</title></head><body>");
        writer.write("<strong>" + result + "</strong>");
        writer.write("</body></html");
    }
}
