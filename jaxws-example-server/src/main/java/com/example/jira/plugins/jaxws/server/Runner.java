package com.example.jira.plugins.jaxws.server;

import javax.xml.ws.Endpoint;

/**
 * Simple class that can be used to publish the {@link HelloWorldService} end-point.
 */
public class Runner
{
    public static void main(String[] args)
    {
        Endpoint.publish("http://localhost:9999/hello", new HelloWorldServiceImpl());
    }
}
