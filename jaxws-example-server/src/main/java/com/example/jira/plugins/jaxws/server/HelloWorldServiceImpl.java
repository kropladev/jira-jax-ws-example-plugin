package com.example.jira.plugins.jaxws.server;

import javax.jws.WebService;


/**
 * Provides a concrete implementation of {@link HelloWorldService}
 */
@WebService(endpointInterface =  "com.example.jira.plugins.jaxws.server.HelloWorldService")
public class HelloWorldServiceImpl implements HelloWorldService
{
    @Override
    public String sayHello(String name)
    {
        if (name == null || name.length() == 0)
        {
            name = "world";
        }

        return "Hello, " + name;
    }
}
