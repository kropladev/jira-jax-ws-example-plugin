package com.example.jira.plugins.jaxws.server;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

/**
 * The JAX-WS Service interface that will be published as a SOAP Endpoint.
 */
@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface HelloWorldService
{
    /**
     * An example SOAP Method that can be called to prove that the JIRA plugin is working.
     */
    @WebMethod
    String sayHello(String name);
}
